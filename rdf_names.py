# RDF
RDF = 'rdf:rdf'
RESOURCE = 'rdf:resource'
ABOUT ='rdf:about'
# Z
COLLECTION = 'z:collection'
# DC
TITLE = 'dc:title'
# DCTERMS
ABSTRACT = 'dcterms:abstract'