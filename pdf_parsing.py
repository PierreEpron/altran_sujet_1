import numpy as np
import pandas as pd

from pdf import PdfFile
from pathlib import Path

from sklearn.preprocessing import FunctionTransformer
from sklearn.pipeline import Pipeline
import glob, re, joblib


RE_ABSTRACT = re.compile(r'a\s?b\s?s\s?t\s?r\s?a\s?c\s?t\s?', re.I)
RE_WORD = re.compile(r'(?u)\b\w\w+\b')
RE_DOT = re.compile(r'\.')
RE_MAJ = re.compile((r'[A-Z]'))

def get_pdf_info():
    df = pd.DataFrame(glob.glob('../biblio/*/files/*/*.pdf'), columns=['path'])
    df['path'] = df['path'].apply(lambda x: Path(x))
    df['stem'] = df['path'].apply(lambda x: x.stem)
    return df

def generate_csv(path):
    csv_path = path.with_suffix('.csv')
    if csv_path.is_file() is False:
        try:
            PdfFile(str(path)).get_data().to_csv(path.with_suffix('.csv'), index=False)
        except Exception as e:
            print(e)

def generate_multiple_csv(paths):
    path_count = len(paths)
    for i in range(path_count):
        path = paths[i]
        if path.suffix == '.pdf':
            print('{} / {} - {:2f} - {}'.format(i, path_count, (i-1) / path_count * 100, path))
            generate_csv(path)

def get_multiple_csv(paths, abstract_re=RE_ABSTRACT):
    result = []
    for path in paths:
        csv_path = path.with_suffix('.csv')
        if csv_path.is_file():
            try:
                tmp = pd.read_csv(csv_path)
                tmp['is_abstract'] = tmp['title'].apply(lambda x: 0 if abstract_re.match(str(x)) is None else 1) 
                if tmp['is_abstract'].sum() > 0:
                    result.append((path, tmp))
            except Exception as e:
                print('{} - {}'.format(path, e))
    return pd.DataFrame(result, columns=['path', 'data'])

# Pipeline

def text_conversion(X):
    X['text'] = X['text'].apply(lambda x: str(x))
    return X

def char_count (X):
    X['char_count'] = X['text'].apply(lambda x: len(RE_DOT.findall(x)))
    return X

def word_count(X):
    X['word_count'] = X['text'].apply(lambda x: len(RE_WORD.findall(x)))
    return X

def dot_count(X):
    X['dot_count'] = X['text'].apply(lambda x: len(RE_DOT.findall(x)))
    return X

def maj_count(X):
    X['maj_count'] = X['text'].apply(lambda x: len(RE_MAJ.findall(x)))
    return X

def select_features(X, feature_names=''):
    return X[feature_names]

def fill_font_na(X):
    X['title_font_size'].fillna(0, inplace=True)
    X['paragraph_font_size'].fillna(0, inplace=True)
    return X

def get_pipeline(feature_names, verbose=0):
    pipeline = Pipeline([
        ('text_conversion', FunctionTransformer(text_conversion)),
        ('char_count', FunctionTransformer(char_count)),
        ('word_count', FunctionTransformer(word_count)),
        ('dot_count', FunctionTransformer(dot_count)),
        ('maj_count', FunctionTransformer(maj_count)),
        ('select_features', FunctionTransformer(select_features)),
        ('fill_font_na', FunctionTransformer(fill_font_na)),
    ], verbose=verbose)
    pipeline.set_params(select_features__kw_args={'feature_names':feature_names})
    return pipeline 

# Model

class AbstractModel:
    def __init__(self, model_path, scaler_path, feature_names, treshold=.38):
        self.model = joblib.load(model_path)
        self.scaler = joblib.load(scaler_path)
        self.pipeline = get_pipeline(feature_names)
        self.treshold = treshold

    def get_abstract(self, pdf_path):
        df = self.get_df(pdf_path)
        if df is None:
            return None
        X = self.scaler.transform(self.pipeline.fit_transform(df))
        y = self.model.predict_proba(X)
        y = np.vectorize(lambda x: True if x > self.treshold else False)(y[:, 1])
        return ' '.join(df[y]['text'])

    def get_df(self, pdf_path):
        csv_path = pdf_path.with_suffix('.csv')
        if csv_path.is_file():
            generate_csv(pdf_path)
        try:
            df = pd.read_csv(pdf_path.with_suffix('.csv'))
        except Exception as e:
            df = None
            print(e)
        return df