from bs4 import BeautifulSoup
import numpy as np
import pandas as pd
import rdf_names as rdfn
import re
import tools

# Load functions

def get_biblio_paths(root, names):
    paths = []
    for name in names:
        path = tools.get_path(root, name)
        if path is not None:
            paths.append(path)
    return paths

def load_biblios(root, names):
    dfs = []
    for path in get_biblio_paths(root, names):
        dfs.append(load_biblio((path / path.parts[-1]).with_suffix('.rdf')))
    return pd.concat(dfs, ignore_index=True)

def load_biblio(rdf_path):
    soup = load_soup(rdf_path)
    indices, resources = index_resources(soup, [rdfn.COLLECTION])     
    return make_frame(soup, indices, resources, rdf_path)

def load_soup(path):
    with open(path, 'r', encoding='utf-8') as f:
        soup = BeautifulSoup(f.read(), features='lxml')
    return soup

def has_resource(tag):
    return tag.has_attr(rdfn.RESOURCE)

def index_resources(soup, exclude_tags=[]):
    indices = {}
    resources = {}
    i = 0
    for child in soup.find(rdfn.RDF).children:
        if child.name != None and child.name not in exclude_tags:         
            keys = [child[rdfn.ABOUT]]
            keys += [tag[rdfn.RESOURCE] for tag in child.findAll(has_resource)]

            new_i = i          
            for key in keys:
                if key in indices.keys():
                    new_i = indices[key]
                    break            

            for key in keys:
                indices[key] = new_i

            if i == new_i:
                i += 1

            resources[keys[0]] = child

    return indices, resources

def make_frame(soup, indices, resources, origin_value):
    M = np.empty((np.max(list(indices.values())) + 1, 3), dtype=object)
    M[:, 0] = origin_value
    M[:, 1] = [[] for i in range(M.shape[0])]
    M[:, 2] = [[] for i in range(M.shape[0])]

    for key, value in indices.items():
        M[value][1].append(key)
        if key not in resources:
            continue            
        M[value][2].append(resources[key])
    return pd.DataFrame(M, columns=['origin', 'keys', 'soups'])

# Feature functions

def extract_feature(df, name, extractor, **kwargs):
    df[name] = df['soups'].apply(extractor, **kwargs)
    return df

def extract_text(x, **kwargs):
    for x in x:
        tmp = x.find(kwargs['tag'], recursive=False)
        if tmp is not None:
            return tmp.text.strip()
    return None

def extract_title(df):
    extract_feature(df, 'title', extract_text, tag=rdfn.TITLE)

def extract_abstract(df):
    extract_feature(df, 'abstract', extract_text, tag=rdfn.ABSTRACT)

def extract_file_path(df):
    df['path'] = df.apply(lambda x: tools.get_first_path(x['origin'].parent, x['keys']), axis=1)

if __name__ == "__main__":
    import timeit
    print(timeit.timeit(lambda:print(load_biblio(r'biblio\1_navigation\1_navigation.rdf').head()), number=1))
    print(timeit.timeit(lambda:print(load_biblio(r'biblio\3_systemes\3_systemes.rdf').head()), number=1))
    print(timeit.timeit(lambda:print(load_biblio(r'biblio\4_r_and_d\4_r_and_d.rdf').head()), number=1))

  