import numpy as np
from pathlib import Path
from langdetect import detect
import os

def get_file_name(path):
    return Path(path).stem

def extract_lang(x):
    if x != '':
        return detect(x)
    else:
        return None

def get_path(root, name):
    path = Path(root) / name
    if path.is_file() or path.is_dir():
        return path
    else:
        print('Path {} point on nothin')
        return None

def get_first_path(root, keys):
    for key in keys:
        path = Path('{}/{}'.format(root, key))
        if os.path.isfile(path):
            return path
    return None

def remove_partial_dup(df, dup_col, keep_cols):
    for row in df[df.duplicated(subset=dup_col)].iterrows():
        kept = df[df[dup_col] == row[1][dup_col]].index[0]
        for col in keep_cols:
            if df.loc[kept, col] is None and row[1][col] is not None:
                df.loc[kept, col] = row[1][col]
    return df.drop_duplicates(subset=[dup_col])
    